package mx.lania.ejemplo.ciudades.control;

import java.util.ArrayList;
import java.util.List;
import mx.lania.ejemplo.ciudades.entidades.Country;
import mx.lania.ejemplo.ciudades.oad.OadCities;
import mx.lania.ejemplo.ciudades.oad.OadCountries;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author jaguilar
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ControladorCatalogosTest {
    
    @Mock
    OadCountries oadCountries;
    
    @Mock
    OadCities oadCities;
    
    @Autowired
    @InjectMocks
    ControladorCatalogos ctrlCatalogos;
    
    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testGetPaises() {
        List<Country> countries = new ArrayList<>();
        countries.add(new Country("ABC"));
        
        when(oadCountries.findAll())
                .thenReturn(countries);
        
        List<Country> resultado = ctrlCatalogos.getPaises();
        assertThat(resultado).isNotEmpty();
        assertThat(resultado.get(0)).hasFieldOrPropertyWithValue("code", "ABC");
    }
}